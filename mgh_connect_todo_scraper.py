import os
import time
import subprocess
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from dateparser import parse


def scrape(driver):
    script_location = os.path.abspath(os.path.dirname(__file__))
    config_path = os.path.join(script_location, "./config.json")
    with open(config_path, "r", encoding="utf-8") as file:
        config = json.loads(file.read())["connect"]

    driver.get(config["url"])
    time.sleep(5)
    uname = driver.find_element("id", "login-email")
    uname.clear()
    uname.send_keys(config["username"])
    pswd = driver.find_element("id", "login-password")
    pswd.clear()
    pswd.send_keys(config["password"])
    submit_button = driver.find_element("id", "login-submit-btn")
    submit_button.click()
    time.sleep(8)
    driver.switch_to.frame(driver.find_element(By.TAG_NAME, "iframe"))
    parsed_titles = driver.find_elements(By.CLASS_NAME, "assignment-title")
    parsed_deadlines = driver.find_elements(By.CLASS_NAME, "due-date")
    existing_todos = json.loads(
        subprocess.check_output(
            [f"todo --porcelain list {config['listName']} --status ANY"],
            shell=True
        )
    )

    for parsed_title, parsed_deadline in zip(parsed_titles, parsed_deadlines):
        parsed_title = parsed_title.text
        parsed_title = parsed_title.replace("\nAssignment", "")
        parsed_deadline = parsed_deadline.text
        parsed_deadline = parsed_deadline.replace("Start: ", "")
        start, end = parsed_deadline.split("  Due: ")
        start = parse(start)
        end = parse(end)
        formatted_start = start.strftime('%m/%d/%y %H:%M')
        formatted_end = end.strftime('%m/%d/%y %H:%M')
        if all(existing_todo["summary"] != parsed_title for existing_todo in existing_todos):
            print(parsed_title)
            subprocess.run(
                [f'echo "Added from MGH Connect" | todo new --read-description --list {config["listName"]} "{parsed_title}" --priority low --start "{formatted_start}" --due "{formatted_end}"'],
                shell=True
            )
        else:
            print("Assignment exists")


if __name__ == "__main__":
    driver = webdriver.Firefox()
    try:
        scrape(driver)
    finally:
        driver.quit()
    subprocess.run(["vdirsyncer sync"], shell=True)
