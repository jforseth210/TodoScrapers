import os
import subprocess
import json
from selenium import webdriver


def scrape(driver):
    script_location = os.path.abspath(os.path.dirname(__file__))
    config_path = os.path.join(script_location, "./config.json")
    with open(config_path, "r", encoding="utf-8") as file:
        config = json.loads(file.read())["webwork"]
    driver.get(config["url"])
    uname = driver.find_element("id", "uname")
    uname.clear()
    uname.send_keys(config["username"])
    pswd = driver.find_element("id", "pswd")
    pswd.clear()
    pswd.send_keys(config["password"])
    submit_button = driver.find_element("id", "none")
    submit_button.click()
    assignments = driver.find_elements("class name", "hardcopy-link")
    possible_status_elements = driver.find_elements("tag name", "td")
    statuses = [
        status
        for status in possible_status_elements
        if ("open" in status.text
            or "closed" in status.text)
        and "will open" not in status.text
    ]

    existing_todos = json.loads(
        subprocess.check_output(
            [f"todo --porcelain list {config['listName']} --status ANY"],
            shell=True
        )
    )

    for i, assignment in enumerate(assignments):
        if "open, due" in statuses[i].text:
            assignment = assignment.text
            assignment = assignment.replace("Download ", "")
            assignment = assignment.replace("Chapter", "Chapter ")
            assignment = f"{assignment} WebWork"
            if all(i["summary"] != assignment for i in existing_todos):
                print(assignment)
                status = statuses[i].text.replace("open, due ", "")
                status = status.replace(" at ", " ")
                status = status.replace(f"pm {config['timezone']}", " PM")
                status = status.replace(f"am {config['timezone']}", " AM")
                print(status)
                subprocess.run(
                    [f'echo "Added from WebWork" | todo new --read-description --list {config["listName"]} "{assignment}" --priority low --due "{status}"'],
                    shell=True
                )


if __name__ == "__main__":
    driver = webdriver.Firefox()
    try:
        scrape(driver)
    finally:
        driver.quit()
    subprocess.run(["vdirsyncer sync"], shell=True)
