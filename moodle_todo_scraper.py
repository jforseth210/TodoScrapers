import os
import subprocess
import json
from ics import Calendar
import requests
import hashlib
import base64


def scrape():
    script_location = os.path.abspath(os.path.dirname(__file__))
    config_path = os.path.join(script_location, "./config.json")
    with open(config_path, "r", encoding="utf-8") as file:
        config = json.loads(file.read())["moodle"]
    calendar = Calendar(requests.get(config["url"]).text)
    existing_todos = []
    for existing_calendar in config["courseFriendlyNames"].values():
        calendars_existing_todos = json.loads(
            subprocess.check_output(
                [f"todo --porcelain list {existing_calendar} --status ANY"],
                shell=True
            )
        )
        for existing_todo in calendars_existing_todos:
            existing_todos.append(existing_todo)
    for i in calendar.events:
        assignment_name = i.name
        assignment_id = hash(i.name)
        hasher = hashlib.sha1(i.name.encode("utf-8"))
        assignment_id = base64.urlsafe_b64encode(
            hasher.digest()[:3]).decode("utf-8")
        assignment_exists = False
        for existing_todo in existing_todos:
            if (assignment_id in existing_todo["description"]):
                assignment_exists = True
        if (not assignment_exists and ("closes" in i.name or "is due" in i.name)):

            # Deal with Moodle weirdness
            assignment_name = assignment_name.replace(" is due", "")
            assignment_name = assignment_name.replace(" closes", "")

            # Deal with professor weirdness
            assignment_name = assignment_name.replace(" Dropbox", "")
            assignment_name = assignment_name.replace("Dropbox for", "")

            for cat in i.categories:
                course_name = config['courseFriendlyNames'][cat]

                # Deal with individual professor weirdness
                if course_name == "intro-to-linux":
                    assignment_name = assignment_name.split(" - ")[0]

                # Guess priority
                priority = "low"
                for keyword, priority_inference in config["priorityInferences"].items():
                    if keyword.lower() in i.name.lower():
                        priority = priority_inference
                # Create task
                subprocess.run(
                    [f'echo "Added from Moodle\nUnique ID: {assignment_id}" | todo new --read-description --list {course_name} "{assignment_name}" --priority {priority} --due "{i.end.to("local").format("MM/DD/YYYY HH:mm")} --"'],
                    shell=True
                )


if __name__ == "__main__":
    scrape()
    subprocess.run(["vdirsyncer sync"], shell=True)
