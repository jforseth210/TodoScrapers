import subprocess
import os
import time
from selenium import webdriver
import mgh_connect_todo_scraper
import webwork_todo_scraper
import moodle_todo_scraper
if __name__ == "__main__":
    driver = webdriver.Firefox()
    try:
        mgh_connect_todo_scraper.scrape(driver)
        webwork_todo_scraper.scrape(driver)
        moodle_todo_scraper.scrape()
    finally:
        driver.quit()
    subprocess.run(["vdirsyncer sync"], shell=True)
    print("Deleting the log in 5 seconds. Press CTRL-C to cancel")
    time.sleep(5)
    os.remove("geckodriver.log")
    print("Log deleted")
