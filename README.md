# Todo Scrapers

A couple little Selenium scrapers I whipped together to pull homework assignments from McGraw Hill Connect and Webwork and add them to my caldav todo list using todoman and vdirsyncer.

All Rights Reserved. Open in issue if you're interested in using, forking, or contributing.
